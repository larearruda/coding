# Assign your variables below, each on its own line!
caesar = "Graham"
praline = "John"
viking = "Teresa"



# Put your variables above this line

print caesar
print praline
print viking

# There are some characters that cause problems. For example:
# 'There's a snake in my boot!'
# This code breaks because Python thinks the apostrophe in 'There's' ends the string. We can use the backslash to fix the problem, like this:
# 'There\'s a snake in my boot!'
# The string below is broken. Fix it using the escape backslash!

'This isn\'t flying, this is falling with style!'