"""
The string "PYTHON" has six characters,
numbered 0 to 5, as shown below:

+---+---+---+---+---+---+
| P | Y | T | H | O | N |
+---+---+---+---+---+---+
  0   1   2   3   4   5

So if you wanted "Y", you could just type
"PYTHON"[1] (always start counting from 0!)

OU SEJE A GNT CONSEGUE INDICAR QUAL CHAR DA STRING TEM QUE GUARDAR
"""
fifth_letter = "MONTY"[4]

print fifth_letter

"""   
Methods that use dot notation only work with strings.

On the other hand, len() and str() can work on other data types.

"""