/* ******************************
verificando propriedades das variaveis e das strings

 ******************************* */


//---------------------------
var anObj = { job: "I'm an object!" };
var aNumber = 42;
var aString = "I'm a string!";

console.log(typeof anObj); // should print "object"
console.log(typeof aNumber); // should print "number"
console.log(typeof aString); // should print "string"


//---------------------------
var myObj = {
    name: "larissa"    
};

console.log( myObj.hasOwnProperty('name') ); // should print true
console.log( myObj.hasOwnProperty('nickname') ); // should print false



//-------------------
var nyc = {
    fullName: "New York City",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

for(var property in nyc){ // exibe os nomes das propriedades do objeto
    console.log(property); 
}


//-------------------
var nyc = {
    fullName: "New York City",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

// write a for-in loop to print the value of nyc's properties
var a = "fullName";
var b = "mayor";
var c = "population";
var d = "boroughs";

var x =[ a, b, c, d]
 