//objetos em javascript
var phonebookEntry = {};

phonebookEntry.name = 'Oxnard Montalvo';
phonebookEntry.number = '(555) 555-5555';
phonebookEntry.phone = function() {
  console.log('Calling ' + this.name + ' at ' + this.number + '...');
};

phonebookEntry.phone();

/*
criando objeto com literal notation (notação literal)
*/
var me = {
    name: 'Larissa',
    age: '20'
};

/* 
criando objeto pelo construtor
*/

var me = new Object();
me.name = 'Larissa';
me.age = 20;

var object1 = new Object();
object1.name = 'larissa';

var object2 = new Object();
object2.name = object1.name;

var object3 = {
    name: 'larissa'
}


var bob = {
  name: "Bob Smith",
  age: 30
};
var susan = {
  name: "Susan Jordan",
  age: 25
};
// here we save Bob's information
var name1 = bob.name;
var age1 = bob.age;
// finish this code by saving Susan's information
var name2 = susan.name;
var age2 = susan.age;



// Take a look at our next example object, a dog
var dog = {
  species: "greyhound",
  weight: 60,
  age: 4
};

var species = dog["species"];
// fill in the code to save the weight and age using bracket notation
var weight = dog.weight;
var age = dog.age;

// ----------------------------------------
// Our bob object again, but made using a constructor this time 
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;
// Here is susan1, in literal notation
var susan1 = {
  name: "Susan Jordan",
  age: 24
};


// -----------------------------------
// help us make snoopy using literal notation
// Remember snoopy is a "beagle" and is 10 years old.
var snoopy = {
    species: "beagle",
    age: 10
};

// help make buddy using constructor notation
// buddy is a "golden retriever" and is 5 years old
var buddy = new Object();
buddy.species = "golden retriever";
buddy.age = 5;

// -----------------------------------
var bicycle = {
    speed: 0,
    gear: 1,
    frame_material: "carbon fiber"
};