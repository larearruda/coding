// -------------------------
// Accepts a number x as input and returns its square
var square = function (x) {
  return x * x;
};

// Write the function multiply below
// It should take two parameters and return the product
var multiply = function(x,y){
    return x * y;
}

multiply(2,4);

// -----------------------
/* **
So What's a Method?

In the last section, we discussed properties. We can think of properties as variables associated with an object. 
Similarly, a method is just like a function associated with an object.

Let's look at bob, our same person object from the last lesson. 
Instead of just having the properties name and age (line 3 & 4), bob also has a method called setAge (line 6). 
As you can probably guess, this method sets bob's age to whatever argument you give it.

Notice how we define setAge kind of like we define a property. 
The big difference is that we put in a function after the equals sign instead of a string or number.

We call a method like a function, but we use ObjectName.methodName(). 
Look at line 10 where we use the method to change bob's age to 40. We did this by calling bob.setAge(40);.
** */

// here is bob again, with his usual properties
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};
// here we set bob's age to 40
bob.setAge(40);
// bob's feeling old.  Use our method to set bob's age to 20
bob.setAge(20);


//-----
var bob = new Object();
bob.age = 17;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};

bob.getYearOfBirth = function () {
  return 2014 - bob.age;
   /*
   ou se precisar pegar a data exata do sistema
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = mm+'/'+dd+'/'+yyyy;
	document.write(today);

   */
};

console.log(bob.getYearOfBirth());


// ------------------------------------------
/* **
Here we have defined an object rectangle starting on line 1. 
It has a two properties, height and width, which represents the height and width of the shape.

We have written a setHeight method which will update rectangle's height to the given parameter. 
This is very similar to setAge from our person example.

Note we have used the keyword this. this is still a placeholder, but in this scenario, 
this can only ever refer to rectangle because we defined setHeight to be explicitly part of rectangle by defining it as rectangle.setHeight. 
** */
var rectangle = new Object();
rectangle.height = 3;
rectangle.width = 4;
// here is our method to set the height
rectangle.setHeight = function (newHeight) {
  this.height = newHeight;
};
// help by finishing this method
rectangle.setWidth = function (newWidth){
    this.width = newWidth;
}
  
// here change the width to 8 and height to 6 using our new methods
rectangle.setWidth(8);
rectangle.setHeight(6);





// -----------------------------
var square = new Object();
square.sideLength = 6;
square.calcPerimeter = function() {
  return this.sideLength * 4;
};
// help us define an area method here

square.calcArea = function(){
  return (this.calcPerimeter() / 4)  * (this.calcPerimeter() / 4);
};


var p = square.calcPerimeter();
var a = square.calcArea();


