function Dog (breed) {
  this.breed = breed;
}

// here we make buddy and teach him how to bark
var buddy = new Dog("Golden Retriever");
buddy.bark = function() {
  console.log("Woof");
};
buddy.bark();

// here we make snoopy
var snoopy = new Dog("Beagle");


// we need you to teach snoopy how to bark here
snoopy.bark = function() {
  console.log("Woof");
};
// this causes an error, because snoopy doesn't know how to bark!
snoopy.bark();


// ---------------------------------

// HERANÇA
// the original Animal class and sayName method
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Hi my name is " + this.name);
};

// define a Penguin class
function Penguin(name){
    this.name = name;
    this.numLegs = 2;
}

Penguin.prototype = new Animal();
penguin.sayName();


// ------------------------------------


var languages = {
    english: "Hello!",
    french: "Bonjour!",
    notALanguage: 4,
    spanish: "Hola!"
};

// print hello in the 3 different languages
for(var x in languages){ //lista o conteudo das propriedades
	if(typeof languages[x]=== 'string'){
	    console.log(languages[x])
	}
}


