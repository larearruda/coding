# linha de comentario

=begin
	bloco de comentário
=end
my_name = "Larissa"
my_age = 20

puts my_name.upcase
puts my_name.downcase
puts my_name.reverse

sum = 13 + 379
product = 923 * 15
quotient = 13209 / 17

# ------------- interação na linha de comando

print "What's your first name?"
first_name = gets.chomp
first_name.capitalize!

print "What's your last name?"
last_name = gets.chomp
last_name.capitalize!

print "What city are you from?"
city = gets.chomp
city.capitalize!

print "What state or province are you from?"
state = gets.chomp
state.upcase!

puts "Your name is #{first_name} #{last_name} and you're from #{city}, #{state}!"